'use strict';

// ASCII only
function bytesToString(buffer)
{
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
}

// ASCII only
function stringToBytes(string)
{
    var array = new Uint8Array(string.length);

    for(var i = 0, l = string.length; i < l; i++)
    {
        array[i] = string.charCodeAt(i);
    }

    return array.buffer;
}

var exofinger =
{
    serviceUUID: '6e400001-b5a3-f393-e0a9-e50e24dcca9e',
    txCharacteristic: '6e400002-b5a3-f393-e0a9-e50e24dcca9e', // transmit is from the phone's perspective
    rxCharacteristic: '6e400003-b5a3-f393-e0a9-e50e24dcca9e'  // receive is from the phone's perspective
};

var app =
{
    initialize: function()
    {
        document.addEventListener("deviceReady", app.onDeviceReady, false);
        $("#refreshButton").on("click", app.refreshDeviceList);
        $("#openButton").on("click", app.open);
        $("#closeButton").on("click", app.close);
        $("#courseButton").on("click", app.setCourse);
        $("#disconnectButton").on("click", app.disconnect);
        $("#course").on("input", app.updateCourse);
    },

    onDeviceReady: function()
    {
        app.refreshDeviceList();
    },

    refreshDeviceList: function()
    {
        $("#deviceList").html(""); // empties the list
        
        ble.scan([exofinger.serviceUUID], 5, app.onDiscoverDevice, app.onError);
    },

    onDiscoverDevice: function(device)
    {
        if(device.name == "ExoFinger")
        {
            var listItem = $("<li/>").data("deviceId", device.id)
                                     .html(device.name + "&nbsp;" +
                                        "(RSSI: " + device.rssi + "dBm&nbsp;|&nbsp;" +
                                        device.id + ")")
                                     .on("click", app.connect);
            $("#deviceList").append(listItem);
        }
    },

    connect: function(e)
    {
        var target = $(e.target)
        var deviceId = $(e.target).data("deviceId")

        function onConnect(peripheral)
        {
            app.determineWriteType(peripheral);

            // subscribe for incoming data
            ble.startNotification(deviceId, exofinger.serviceUUID, exofinger.rxCharacteristic, app.onData, app.onError);
            
            $("#disconnectButton").data("deviceId", deviceId);
            
            target.addClass("connected")
        };

        // If not already connected, connect to the selected device
        if(!$("#disconnectButton").data("deviceId"))
        {
            ble.connect(deviceId, onConnect, app.onError);
        }
    },

    determineWriteType: function(peripheral)
    {
        var characteristic = peripheral.characteristics.filter(function(element)
        {
            if(element.characteristic.toLowerCase() === exofinger.txCharacteristic)
            {
                return element;
            }
        })[0];

        app.writeWithoutResponse = (characteristic.properties.indexOf('WriteWithoutResponse') > -1);
    },

    onData: function(data)
    {
        var buffer = new Uint8Array(data);

        if(buffer[0])
        {
            $("#closing").addClass("true");
        }
        else
        {
            $("#closing").removeClass("true");
        }
        
        if(buffer[1])
        {
            $("#done").addClass("true");
        }
        else
        {
            $("#done").removeClass("true");
        }

        var pwm = (buffer[2] << 8) + buffer[3];
        var course = (buffer[4] << 24) + (buffer[5] << 16) + (buffer[6] << 8) + buffer[7];
        var position = (buffer[8] << 24) + (buffer[9] << 16) + (buffer[10] << 8) + buffer[11];

        $("#pwm").html(pwm);
        $("#position").html(position);
    },

    sendData: function(data)
    {
        var deviceId = $("#disconnectButton").data("deviceId");
        
        function success()
        {
        };

        function failure(reason)
        {
            alert("Failed writing data to ExoFinger " + JSON.stringify(reason));
        };

        if(deviceId)
        {
            if(app.writeWithoutResponse)
            {
                ble.writeWithoutResponse(
                    deviceId,
                    exofinger.serviceUUID,
                    exofinger.txCharacteristic,
                    stringToBytes(data), success, failure
                );
            }
            else
            {
                ble.write(
                    deviceId,
                    exofinger.serviceUUID,
                    exofinger.txCharacteristic,
                    stringToBytes(data), success, failure
                );
            }
        }
    },

    open: function(event)
    {
        app.sendData("O");
    },

    close: function(event)
    {
        app.sendData("C");
    },

    setCourse: function(event)
    {
        app.sendData("L" + parseFloat($("#courseDisplay").html()))
    },

    disconnect: function(event)
    {
        var deviceId = $("#disconnectButton").data("deviceId");

        if(deviceId)
        {
            ble.disconnect(deviceId, app.disconnected, app.onError);
        }
    },

    disconnected: function()
    {
        $("#deviceList > li").removeClass("connected");
        $("#disconnectButton").data("deviceId", null);
    },

    onError: function(reason)
    {
        alert("ERROR: " + JSON.stringify(reason)); // real apps should use notification.alert
    },

    updateCourse: function()
    {
        $("#courseDisplay").html($(this).val() + "cm");
    }
};

app.initialize();