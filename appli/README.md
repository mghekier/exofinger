## Application Exofinger

* 29/07/2021
* https://cordova.apache.org/

## Installation (Ubuntu 20.04)

* installation [android-studio](https://developer.android.com/studio) (articfox, 2020.3.1)
* `sudo apt install gradle` (v 4.4 avec la 7. ca ne fonctionne pas)
* passer en java 1.8 `update-alternatives --config java` et `update-alternatives --config javac`

## Variable d'environnement

```bash
$> export ANDROID_SDK_ROOT=/home/.../Android/Sdk
$> export PATH=${PATH}:/home/.../Android/Sdk/tools
```

## Build APK (dans le projet `exofinger/appli`)

```bash
$> cordova platform add android
$> cordova build android
```
