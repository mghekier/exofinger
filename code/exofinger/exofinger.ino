#include <bluefruit.h>
#include "RotaryEncoder.h"


#define CM (2500) // Number of ticks per centimeter

#define DEFAULT_POS_CLOSED  (4 * CM)

#define THRES_ERROR (10)
#define P           (2.0)
#define I           (1.0)
#define DT          (0.01)
#define ALPHA       (0.7)
#define INT_MAX     (1000.0)

#define ENC_C1      A0
#define ENC_C2      A1 
#define ENC_VCC     A2

#define MOT_GND     10 // D10 
#define MOT_VCC     11 // D11
#define MOT_EN      12 // D12
#define MOT_PH      13 // D13

#define MIN_PWM     85 // 85 / 512 -> 16.6%

// To connect a phone or PC using UART interface
BLEUart bleuart;

// To connect BBBB (Big Blue Bluetooth Button)
BLEClientHidAdafruit hid;

// Last checked report, to detect if there is changes between reports
hid_keyboard_report_t last_kbd_report = { 0 };

bool closed = false;
bool arrived = true;
int pos_closed = DEFAULT_POS_CLOSED;
char output_buf[12];

void setup()
{
  Serial.begin(115200);

  // Initialize Bluefruit with maximum connections as Peripheral = 1, Central = 1
  Bluefruit.begin(1, 1);
  
  Bluefruit.setName("ExoFinger");

  // Initialize BLE UART service
  bleuart.begin();
  bleuart.setRxCallback(uart_rx_callback);

  Bluefruit.Periph.setConnectCallback(prph_connect_callback);
  Bluefruit.Periph.setDisconnectCallback(prph_disconnect_callback);

  // Init BLE Central Hid Serivce
  hid.begin();

  hid.setKeyboardReportCallback(keyboard_report_callback);

  // Increase Blink rate to different from PrPh advertising mode
  Bluefruit.setConnLedInterval(250);

  // Callbacks for Central
  Bluefruit.Central.setConnectCallback(connect_callback);
  Bluefruit.Central.setDisconnectCallback(disconnect_callback);

  /* Start Central Scanning
   * - Enable auto scan if disconnected
   * - Interval = 100 ms, window = 80 ms
   * - Don't use active scan
   * - Filter only accept HID service in advertising
   * - Start(timeout) with timeout = 0 will scan forever (until connected)
   */
  Bluefruit.Scanner.setRxCallback(scan_callback);
  Bluefruit.Scanner.restartOnDisconnect(true);
  Bluefruit.Scanner.setInterval(160, 80); // in unit of 0.625 ms
  Bluefruit.Scanner.filterService(hid);   // only report HID service
  Bluefruit.Scanner.useActiveScan(false);
  Bluefruit.Scanner.start(0);             // 0 = Don't stop scanning after n seconds

  // Start advertising
  start_adv();

  // Pin Setup
  pinMode(LED_BLUE, INPUT); // Deactivate blue LED to reduce power consumption

  pinMode(ENC_VCC, OUTPUT); // Motor encoder

  pinMode(MOT_GND, OUTPUT); // Motor driver
  digitalWrite(MOT_GND, 0);
  pinMode(MOT_VCC, OUTPUT);

  disable_motor();
  
  pinMode(MOT_EN, OUTPUT);
  digitalWrite(MOT_EN, 0);
  pinMode(MOT_PH, OUTPUT);
  digitalWrite(MOT_PH, 0);

  // Encoder
  RotaryEncoder.begin(ENC_C1, ENC_C2);
  RotaryEncoder.start();

  // PWM
  HwPWM0.addPin(MOT_EN);
  HwPWM0.begin();
  HwPWM0.setResolution(9); // 9-bit resolution (max is 15 bits)
  HwPWM0.setClockDiv(PWM_PRESCALER_PRESCALER_DIV_1); // freq = 16Mhz => PWM frequency is 31.25kHz
}

void enable_motor(void)
{
  digitalWrite(MOT_VCC, 1);
  digitalWrite(ENC_VCC, 1);
}

void disable_motor(void)
{
  digitalWrite(MOT_VCC, 0);
  digitalWrite(ENC_VCC, 0);  
}

void start_adv(void)
{
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
 
  // Include bleuart 128-bit uuid
  Bluefruit.Advertising.addService(bleuart);
 
  // Secondary Scan Response packet (optional)
  // Since there is no room for 'Name' in Advertising packet
  Bluefruit.ScanResponse.addName();
 
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds
}

void scan_callback(ble_gap_evt_adv_report_t* report)
{
  // Since we configure the scanner with filterUuid()
  // Scan callback only invoked for device with hid service advertised  
  // Connect to the device with hid service in advertising packet
  Bluefruit.Central.connect(report);
}

void prph_connect_callback(uint16_t conn_handle)
{
  (void) conn_handle;
}

void prph_disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
  (void) conn_handle;
  (void) reason;
}

void connect_callback(uint16_t conn_handle)
{
  if(hid.discover(conn_handle))
  {
    if(!Bluefruit.requestPairing(conn_handle))
    {
      return;
    }

    uint8_t hidInfo[4];
    hid.getHidInfo(hidInfo);

    // BLEClientHidAdafruit currently only supports Boot Protocol Mode
    // for Keyboard and Mouse. Let's set the protocol mode on prph to Boot Mode
    hid.setBootMode(true);

    // Enable Keyboard report notification if present on prph
    if(hid.keyboardPresent()) hid.enableKeyboard();
  }
  else
  {
    // disconnect since we couldn't find blehid service
    Bluefruit.disconnect(conn_handle);
  }  
}

void disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
  (void) conn_handle;
  (void) reason;
}

void loop()
{
  static int position = 0;
  static float int_error = 0.0;
  int target = 0;

  // Update position with encoder reading
  position += RotaryEncoder.read();

  if(closed)
  {
    // Closed
    target = pos_closed;
  }
  else
  {
    // Opened
    target = 0;
  }

  // PI computation
  int error = target - position;

  // If error is very small, set it to zero to avoid oscillations
  if(abs(error) < THRES_ERROR)
  {
    error = 0;
  }
  
  float cmd = P * error + I * int_error;

  // Update error integral with dampening
  int_error = ALPHA * (error * DT) + (1 - ALPHA) * int_error;

  if(fabsf(int_error) > INT_MAX)
  {
    if(int_error > 0)
    {
      int_error = INT_MAX;
    }
    else
    {
      int_error = -INT_MAX;
    }
  }

  // Set direction
  if(cmd > 0)
  {
    digitalWrite(MOT_PH, 0);
  }
  else
  {
    digitalWrite(MOT_PH, 1);
  }

  // Set motor "speed": 0 - 511
  int value = (int)fabsf(cmd);

  if(value > 511)
  {
    value = 511;
  }
  
  if(value < MIN_PWM)
  {
    arrived = true;
    disable_motor();
  }
  else
  {
    arrived = false;
    enable_motor();
    HwPWM0.writePin(MOT_EN, value, false);
  }

  //Serial.printf("%d\n", value);

  if(bleuart.notifyEnabled())
  {
    uint8_t cpt = 0;
    
    output_buf[cpt++] = closed;
    output_buf[cpt++] = arrived;
    output_buf[cpt++] = (value >> 8) & 0xFF;
    output_buf[cpt++] = value & 0xFF;
    output_buf[cpt++] = (pos_closed >> 24) & 0xFF;    
    output_buf[cpt++] = (pos_closed >> 16) & 0xFF;
    output_buf[cpt++] = (pos_closed >> 8) & 0xFF;
    output_buf[cpt++] = pos_closed & 0xFF;
    output_buf[cpt++] = (position >> 24) & 0xFF;    
    output_buf[cpt++] = (position >> 16) & 0xFF;
    output_buf[cpt++] = (position >> 8) & 0xFF;
    output_buf[cpt++] = position & 0xFF;

    bleuart.write(output_buf, sizeof(output_buf));
  }  

  delay((int)(DT * 1000));
}

void keyboard_report_callback(hid_keyboard_report_t* report)
{
  // Check with last report to see if there is any changes
  if(memcmp(&last_kbd_report, report, sizeof(hid_keyboard_report_t)))
  {
    if(report->modifier == 2 && report->keycode[0] == 4)
    {
      closed = !closed;
    }
  }

  // update last report
  memcpy(&last_kbd_report, report, sizeof(hid_keyboard_report_t));  
}

void uart_rx_callback(uint16_t dummy)
{
  // Forward data from Mobile to our peripheral
  char str[21] = { 0 };
  int value = 0;
  bleuart.read(str, sizeof(str) - 1);

  switch(str[0])
  {
    case 'O':
      closed = false;
      break;

    case 'C':
      closed = true;
      break;

    case 'L':
      value = (int)(atof(str + 1) * CM);
      Serial.printf("Value: %d\n", value);
      pos_closed = value;
      break;

     default:
      Serial.printf("Unknown command: %s\n", str);
  }
}
