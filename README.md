# Exofinger V2

## Introduction

Exofinger V2 est une version améliorée de [Exofinger V1](https://wikilab.myhumankit.org/index.php?title=Projets:Exofinger_:_Thumb), prototype réalisé lors du [Fabrikarium](https://www.ariane.group/fr/actualites/bienvenue-au-fabrikarium-2020/) du 20 au 22 octobre 2020.

Ce prototype est une aide à la pince de la main gauche en rapprochant le pouce du reste des doigts chez une personne qui peut mobiliser son pouce. Cette orthèse de pouce motorisée doit permettre la préhension d'un objet.

Les améliorations amenées sont mécanique pour augmenter la course de serrage du pouce et rendre le prototype plus utilisable au jour le jour. Pour cela, les principales modifications sont :

* Passer d'un moteur linéaire à un moteur rotatif pour augmenter la course du fil de serrage par l'intermédiaire d'une poulie.
* Développer un module de bouton sans-fil à base de communication Bluetooth Low Energy (ble)
* Utiliser des [AdaFruit feather nRF52840 express](https://www.adafruit.com/product/4062) qui permettent:
  * la communication ble
  * la recharge des batteries depuis leur connecteur USB
* Réduire la taille du boitier
  
![prototype](./figs/exomotor_avec_gant.jpg)

## Les modules

Le prototype est désormais constitué de deux modules qui communiquent sans-fil par ble. Un module (Exomotor) est en charge de contrôler le moteur qui actionne le pouce via le fil en nylon et un autre module (Exobutton) qui gère le bouton pour envoyer l'ordre de serrage/déserrage.

Cette architecture distribuée entre actionneur et déclencheur, pourra être utilisé pour d'autres prototypes.

### Exomotor

![Exomotor](./figs/exomotor.jpg)
#### Electronique

L'Exomotor est architecturé autour d'une carte [AdaFruit feather nRF52840 express](https://www.adafruit.com/product/4062). La carte est alimentée par une batterie [LiPo 1S1P 3.7V 500mAh ICP303450PA](https://www.conrad.fr/p/accu-lithium-polymere-37v-510mah-renata-icp303450pa-1214020). Un [moto-réducteur CC 6V 1:200 avec encodeur intégré](https://fr.aliexpress.com/item/4000381882503.html?spm=a2g0s.9042311.0.0.1ec66c375r4WmL) est commandé par un [driver de moteur DRV8838](https://www.pololu.com/product/2990).

#### Mécanique

Le [moto-réducteur CC 6V 1:200 avec encodeur intégré](https://fr.aliexpress.com/item/4000381882503.html?spm=a2g0s.9042311.0.0.1ec66c375r4WmL) est couplé avec une bobine en V pour enrouler un câble. Celui-ci passe dans une gaine en téflon entre le boitier moteur et le gant, puis dans une autre gaine thermoformée dans le gant, enfin son extrémité est fixée sur le gant au niveau de la phalange distale du pouce.

 L’enroulement de ce câble provoque une force de tension qui entraîne une fermeture du pouce sur l’index. Ainsi l’Exofinger constitue une orthèse motorisée « jointless » c’est-à-dire sans articulation.

Le câble est un fil de pêche standard en Nylon de section 0.6mm – dimensionné pour une « force de tension » de 20kg.

#### Logiciel

Le module est configuré pour être un serveur BLE de type HID et se connecter à Exobutton. Il déclenchera l'action lorsqu'il recevra le caractère 'A' de Exobutton.

Il est également configuré en client BLE de type uart. Cela lui permet de se connecter à une App Android pour lui permettre de changer sa configuration.

Un contrôleur PID permet de contrôler la position du moteur qui actionne le pouce.

### Exobutton

![Exobutton](./figs/exobutton.jpg)
#### Electronique

Le bouton est architecturé autour d'une carte [AdaFruit feather nRF52840 express](https://www.adafruit.com/product/4062). La carte est alimentée par une batterie [LiPo 1S1P 3.7V 500mAh ICP303450PA](https://www.conrad.fr/p/accu-lithium-polymere-37v-510mah-renata-icp303450pa-1214020). Trois [micro-switches](https://www.conrad.fr/p/microrupteur-hartmann-microhart-mdb1-05c01c03a-125-vac-3-a-1-x-onon-a-rappel-1-pcs-704738) montés en parallèle sur entre l'entrée A0 et la masse complètent le schéma électronique ultra-simple du bouton.

#### Mécanique

Le bouton est constitué d'un boîtier cylindrique en trois parties. Le fond du boîtier accueille la batterie et la carte électronique qui sont superposée. La partie supérieure (immobile) du boîtier supporte les trois micro-switches et enfin la partie mobile du bouton est appuyée sur les microswitches et quatre ressorts optionnels et maintenue en place par la partie supérieur du boîtier.

Le port micro-USB de la carte électronique est laissé accessible par une ouverture sur la partie supérieure du boîtier.

Les deux parties immobiles du bouton sont maintenues entre elles par un emboîtement simple. Après fabrication la simple friction des deux parties suffit à maintenir le boîtier fermé. Cependant après plusieurs ouvertures / fermetures la fermeture n'est plus aussi parfaite, on pourrait envisager de mettre quelques vis de fixation sur les flans du boîtier pour limiter le problème, mais il faudra probablement élargir un peu le boîtier.

Les quatres ressorts servent de variable d'ajustement sur la sensibilité / dureté du bouton. Il ne sont mécaniquement pas nécessaires. Les ressorts des micro-switches sont suffisants pour soutenir le bouton (pas de déclenchement à vide), dans ce cas la sensibilité est maximale, mais il ne faut pas avoir la main trop lourde lors de l'activation car les micro-switches prennent les chocs en direct. A l'inverse ajouter quatre ressorts de raideur importante permet de déclencher le bouton par une frappe plus lourde sans risquer d'endommager les micro-switches.

##### Vue du boîtier ouvert

![Boîtier du bouton ouvert](./figs/button2.png)

##### Vue du boîtier ouvert avec la partie mobile du bouton

![Boîtier du bouton sans le capot](./figs/button1.png)

##### Vue du boîtier fermé

![Boîtier du bouton fermé](./figs/button.png)

#### Logiciel

Le bouton est configuré pour être un périphérique BLE de type HID. Lorsqu'il sera connecté à l'Exomotor, il sera vu comme un clavier et chaque pression sur le bouton simulera l'envoi d'un caractère 'A' sur le lien BLE.

Lorsque l'un des micro-switch est fermé, la pin A0 est connectée à la masse et à l'inverse lorsqu'ils sont tous ouverts, la pin A0 est laissée libre. Il faut donc la configurer avec une résistance de pull-up interne pour que la lecture de l'entrée A0 soit à 1 si l'entrée n'est pas connectée à la masse.

Pour le moment, la lecture de l'entrée A0 se fait dans la boucle principale du programme (fonction `loop()`), il pourra être intéressant pour limiter la consommation d'énergie (et ainsi augmenter la durée de vie du bouton sur batterie) de détecter les appuis par interruption (il faudra dans ce cas faire bien attention à traiter correctement les rebonds sur les micro-switches)

## Analyse de risque

* Risques d'inflammation liés aux batteries: [LiPo 1S1P 3.7V 500mAh ICP303450PA](https://www.conrad.fr/p/accu-lithium-polymere-37v-510mah-renata-icp303450pa-1214020). La
[fiche technique](https://asset.conrad.com/media10/add/160267/c1/-/en/001214020DS01/fiche-technique-1214020-accu-lithium-polymere-37v-510mah-renata-icp303450pa.pdf) montre qu'elle dispose d'un circuit de protection et d'une large fourchette de températures de stockage : -20 à +45 degrés. La qualité des batteries limite les risques de détérioration et d'inflammation.
* Risques liés à un défaut de commande moteur: Le [moto-réducteur CC 6V 1:200 avec encodeur intégré](https://fr.aliexpress.com/item/4000381882503.html?spm=a2g0s.9042311.0.0.1ec66c375r4WmL) peut produire au maximum un couple de 2.5kg.cm. Dans le cas le plus défavorable, c’est-à-dire quand le bras de levier entre le couple moteur et la tension du câble est le plus faible, ie quand le câble est le moins enroulé : Tensionmax = couplemax/diamètreBobinemin (sans prendre en compte les forces de frottements qui ne sont pas négligeables ~20-30%) **A COMPLETER** Par ailleurs, le système de transmission n'est pas réversible (câble) il n'y a pas de risque de provoquer un mouvement dans une autre direction ou dans un autre sens.
* Pour un usage prolongé, il existe des risques de TMS lié au mauvais alignement entre l’axe de rotation biologique et celui de l’orthèse. L’Exofinger étant une orthèse sans articulation, il n’y a pas ce risque.
* Le câble parcourt presque tout le tour de la main, une fois en tension celui-ci pourrait entrainer une pression sur la main. C’est pourquoi la gaine dans le gant est semi-rigide et présente un diamètre de **A COMPLETER **qui permet d’absorber et de distribuer la pression du câble sur la main.
* Risque électrique : Les tensions ne dépassant pas 3.7 Volts, il n’y a aucun risque d’électrisation (risque à partir de 20V dans un environnement très humide).
* Risque de prise de contrôle par un tiers : La commande ne répond qu’avec des appareils pairés
