//Timer
#include <SimpleTimer.h>
SimpleTimer timer;
const float dT_mesure = 0.01;
const float dT_control = 0.01;
const float dT_affichage = 0.01;

//Moteur
#define moteurMLI 3
#define moteurSensRotation 12
#define moteurFrein 9

//Codeur de position
#define encoderPinA 18
#define encoderPinB 19

#define baudRate 115200

short compteurCodeurMoteur = 0; // signal interne du codeur incremental
float Pos = 0; // position en degre
void encodeMoteur() {
  if (digitalRead(encoderPinA) == digitalRead(encoderPinB)) {
    compteurCodeurMoteur++;
  }
  else {
    compteurCodeurMoteur--;
  }
  if (compteurCodeurMoteur < 0) {
    compteurCodeurMoteur += 960;
  }
  if (compteurCodeurMoteur > 960) {
    compteurCodeurMoteur -= 960;
  }
  int tmp = 30 * compteurCodeurMoteur / 960;
  Pos = 12 * tmp;
}


int vitesse;
float prevPos = 0;
const float pi = 3.1415;
//Variables pour le filtre
int vitesseTab[16] = {0};
short index = 0;
int accumulateur = 0;

void mesureVitesse() {
  //Calcul des vitesses en cartésien
  float dcos = (cos(pi * Pos / 180) - cos(pi * prevPos / 180)) / dT_mesure;
  float dsin = (sin(pi * Pos / 180) - sin(pi * prevPos / 180)) / dT_mesure;
  prevPos = Pos;
  float vitesseInst = sqrt(dcos * dcos + dsin * dsin);
  if (cos(pi * Pos / 180)*dsin - sin(pi * Pos / 180)*dcos < 0) { //sens de rotation
    vitesseInst = -vitesseInst;
  }
  // filtrage par  moyenne glissante de la vitesse pour limiter les oscillations
  accumulateur += vitesseInst - vitesseTab[index];
  vitesseTab[index] = (int) vitesseInst;
  index = (index == 15) ? 0 : index + 1; // if (index==15) index = 0; else index++;
  vitesse = accumulateur >> 4;
}

int consigneVitesse = 12; // consigne de vitesse rad/s (Ne pas depasser 29rad/s en charge et 36 a vide)
float Kp = 0.1;
float rapportCyclique = 0;
int erreur = 0;
/*Partie intégrale*/
float integraleErreur = 0.;
float Ti = 5;

void control() {
  erreur = consigneVitesse - vitesse;
  /*Partie intégrale non fournie
    integraleErreur += erreur * dT_control; //intégrale
    rapportCyclique = Kp * erreur + integraleErreur / Ti; //Correction */
  rapportCyclique = Kp * erreur;
  rapportCyclique = constrain(rapportCyclique, 0, 1); //saturation
  analogWrite(moteurMLI, (int)(rapportCyclique * 255)); //commande le moteur par MLI
}


void afficher() {
  Serial.print("Kp = ");
  Serial.println(Kp);

  Serial.print("La vitesse est (rad/s):");
  Serial.println(vitesse);
}


void setup() {
  Serial.begin(baudRate); // initialisation de la liaison série

  timer.setInterval((int)1000 * dT_mesure, mesureVitesse); //en seconde
  timer.setInterval((int)1000 * dT_control, control);
  timer.setInterval((int)1000 * dT_affichage, afficher);

  pinMode(moteurSensRotation, OUTPUT); //initialisation direction moteur A
  pinMode(moteurFrein, OUTPUT); //initialisation frein A
  digitalWrite(moteurSensRotation, LOW); //definie le sens de rotation
  digitalWrite(moteurFrein, LOW);   //enleve le frein

//modification des registres pour changer la frequence de la MLI
  int myEraser = 7;
  TCCR3B &= ~myEraser;
  int myPrescaler = 2;
  TCCR3B |= myPrescaler;
  attachInterrupt(digitalPinToInterrupt(encoderPinA), encodeMoteur, CHANGE); //trigger encodeMoteur quand encoderPinA change de valeur
}


void loop() {
  timer.run();

}
