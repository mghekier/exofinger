%% Trace le diagramme de Bode de H
K1 = 79.94;
K2 = -124.9;
tau1 = -1/-1.781;
tau2 = -1/-11.6;
H1 = tf([K1],[tau1 1]);
H2 = tf([K2],[tau2 1]);
H = H1 - H2;
%figure(1)
%stepplot(H); %r�ponse indicielle
figure(2)
%w = [0.5 1 2 4 5 8 10 15 20 23 31 40 44 50 85 100];
%bode(H,w,'.-')
%grid on
%bode(H);
num=[1]; %set the numerator in a matrix
den=[1 1.5]; %set the denominator in a matrix
Transfer_Function=tf(num,den) % use the tf function to set the transfer function
[mag,phase,wout] = bode(H);                     % Get Plot Data
mag = squeeze(mag);                                             % Reduce (1x1xN) Matrix To (1xN)
phase= squeeze(phase);
magr2 = (mag/max(mag)).^2;                                      % Calculate Power Of Ratio Of �mag/max(mag)�
dB3 = interp1(magr2, [wout phase mag], 0.5, 'spline');          % Find Frequency & Phase & Amplitude of Half-Power (-3 dB) Point
figure(1)
subplot(2,1,1)
semilogx(wout, 20*log10(mag), '-b',  dB3(1), 20*log10(dB3(3)), '+r', 'MarkerSize',10)
grid
subplot(2,1,2)
semilogx(wout, phase, '-b',  dB3(1), dB3(2), '+r', 'MarkerSize',10)
grid