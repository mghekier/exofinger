%clear;
close all;
clc;

if ~isempty(instrfind)
    fclose(instrfind);
    delete(instrfind);
end

%% Acquisition des donnees
% 
% s = serial('COM3','BaudRate',115200);
% isvalid(s) %verifie la validite
% instrfind(s); %read serial port
% 
N = 4000; %nbr de points recuperes
instant = linspace(0,N,N); % initialisation de la matrice des instant
% mesure = zeros(1,N); % initilisation de la matrice des mesures
% try
%    fopen(s);
%    for i = 1:N
%       %l = fscanf(s); %prend la premiere ligne
%       mesure(i) = str2double(fscanf(s)); %signal
%       disp(mesure(i));
%   end
% catch % attrape les erreurs afin de fermer la connexion
% warning('Un probleme a ete rencontre')
% end
% % fermeture de la connexion
% fclose(s);
% delete(s)
% clear('s')

%% Traitement des donn�es
%Filtrage
% fs=1/0.01;
% [b,a]=butter(2,30/(fs/2),'low');
% mesure=filter(b,a,mesure);

%Affichage
consigne = zeros(1,N);
consigne = consigne + 200;
temps = instant * 0.001;
plot(temps, mesure, temps, consigne)
xlabel('Temps (s)')
ylabel('Positions (degre)')
legend('Mesure','Consigne')


