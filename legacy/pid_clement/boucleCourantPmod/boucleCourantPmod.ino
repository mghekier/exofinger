#define baudRate 9600
/**Capteur**/
#define Correction 100         // pourcentage de correction de la sensibiliité du capteur                                  
#define Filtre 0               // 
#define Echantillon 5000       // nombre d'échantillons
#define CS 10                  // affectation de la broche CS
//CLK 52 (mega)
//MISO (DO) 50 (mega)
#include <SPI.h>               // appel de la bibliothèque
int MSB;
int LSB;
signed int valeur;
signed int total = 0;
signed int mesure_courant;

/**Moteur**/
#include <SimpleTimer.h>
// routines declenchees regulierement
SimpleTimer timer;
const float dT_mesure = 0.01;
const float dT_control = 0.01;
const float dT_print = 1;
// pins utilisees par le moteur
#define motor_pwm 3
#define motor_direction 12
#define motor_brake 9

void mesureCourant() {
  if (Filtre == 1)
  {
    for (int i; i < Echantillon; i++)
    {
      digitalWrite(CS, LOW);               // activation de la ligne CS
      MSB = SPI.transfer(0x00);            // récupération des bit de poids forts
      LSB = SPI.transfer(0x00);            // récupération des bit de poids faibles
      digitalWrite(CS, HIGH);              // désactivation de la ligne CS
      valeur = (Correction / 100) * (10000  * (((MSB << 8) | LSB) - 2048)) / 899; //formule donnée dans la documentation
      total = total + valeur;
    }
    mesure_courant = total / Echantillon;
  }
  else
  {
    digitalWrite(CS, LOW);               // activation de la ligne CS
    MSB = SPI.transfer(0x00);            // récupération des bit de poids forts
    LSB = SPI.transfer(0x00);            // récupération des bit de poids faibles
    digitalWrite(CS, HIGH);              // désactivation de la ligne CS
    mesure_courant = (Correction / 100) * (10000  * (((MSB << 8) | LSB) - 2048)) / 899; //formule donnée dans la documentation
  }
}

signed int courantMax = 1000; //mA
float Ki = 1; //proportionnel
int err_courant = 0;
void control() {
  err_courant = courantMax - mesure_courant;
  float tension_cons = Ki * err_courant;
  tension_cons = constrain(tension_cons, 0, 1);
  analogWrite(motor_pwm, (int) (tension_cons * 255)); //commande de la rotation par MLI
}

void afficher() {
  Serial.print("err_courant (mA) = ");
  Serial.println(err_courant);

  Serial.print("Courant (mA) = ");
  Serial.println(mesure_courant);
}

void setup() {
  SPI.begin();                          // initialisation du port SPI
  SPI.setDataMode(SPI_MODE0);           // configuration de la liaison SPI en mode 0
  SPI.setClockDivider(SPI_CLOCK_DIV16); // configuration de l'horloge à 1MHz
  pinMode(CS, OUTPUT);

  Serial.begin(baudRate); // initialisation de la liaison série
  //Moteur sur le Channel A
  pinMode(motor_direction, OUTPUT); //initialisation direction du moteur sur A
  pinMode(motor_brake, OUTPUT); //initialisation frein du moteur sur A
  timer.setInterval((int)1000 * dT_mesure, mesureCourant);
  timer.setInterval((int)1000 * dT_control, control);
  timer.setInterval((int)1000 * dT_print, afficher);
  
  digitalWrite(motor_direction, LOW);
  digitalWrite(motor_brake, LOW);   //enleve le frein
  }

void loop() {
  timer.run();
}
