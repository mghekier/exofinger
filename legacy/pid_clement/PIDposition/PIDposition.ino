//Timer
#include <SimpleTimer.h>
SimpleTimer timer;
const float dT_mesure = 0.001;
const float dT_control = 0.001;
const float dT_affichage = 0.001;

//Moteur
#define moteurMLI 3
#define moteurSensRotation 12
#define moteurFrein 9
const int vitesseMax = 29;

//Codeur de position moteur
#define encoderPinA 18
#define encoderPinB 19

#define baudRate 115200

short compteurCodeurMoteur = 0; // signal interne du codeur incremental
float Position = 0; // position en degre
void encodeMoteur() {
  if (digitalRead(encoderPinA) == digitalRead(encoderPinB)) {
    compteurCodeurMoteur++;
  }
  else {
    compteurCodeurMoteur--;
  }
  int tmp = 30 * compteurCodeurMoteur / 960;
  Position = 12 * tmp;
}


int vitesse;
float prevPosition = 0;
const float pi = 3.1415;
//Variables pour le filtre
int vitesseTab[16] = {0};
short index = 0;
int accumulateur = 0;

void mesureVitesse() {
  //Calcul des vitesses lineaires
  float dcos = (cos(pi * Position / 180) - cos(pi * prevPosition / 180)) / dT_mesure;
  float dsin = (sin(pi * Position / 180) - sin(pi * prevPosition / 180)) / dT_mesure;
  prevPosition = Position;
  float vitesseInst = sqrt(dcos * dcos + dsin * dsin);
  if (cos(pi * Position / 180)*dsin - sin(pi * Position / 180)*dcos < 0) { //sens de rotation
    vitesseInst = -vitesseInst;
  }
  // filtrage par  moyenne glissante de la vitesse pour limiter les oscillations
  accumulateur += vitesseInst - vitesseTab[index];
  vitesseTab[index] = (int) vitesseInst;
  index = (index == 15) ? 0 : index + 1; // if (index==15) index = 0; else index++;
  vitesse = accumulateur >> 4;
}

int consignePosition = 200; //en degre
float rapportCyclique = 0;
/** Vitessse **/
float consigneVitesse = 0;
float erreurVitesse = 0;
float Kvitesse = 0.1;
float erreurIntegraleVitesse = 0.0;
float Tivitesse = 5;

/** Position **/
int erreurPosition = 0;
float Kposition = 0.1;
float Tiposition = 0.12;
float Tdposition = 0.14;
float erreurIntegralePosition = 0.0;
float erreurDerivePosition = 0.0;
int prevErreurPosition = 0;
void control() {
  /*Position*/
  erreurPosition = consignePosition - Position;
  erreurIntegralePosition += erreurPosition * dT_control; //integrale
  erreurDerivePosition = (erreurPosition - prevErreurPosition) / dT_control;
  prevErreurPosition = erreurPosition;
  erreurIntegralePosition = constrain(erreurIntegralePosition, -Tiposition, Tiposition); //Antiwindup
  if (abs(erreurPosition) < 5) { //Acceptation d'une marge d'erreur
    consigneVitesse = 0;
  } else {
    consigneVitesse = Kposition * erreurPosition + erreurIntegralePosition / Tiposition + erreurDerivePosition * Tdposition;
  }
  consigneVitesse = constrain(consigneVitesse, -vitesseMax, vitesseMax);//redondant ?
  /*Vitesse*/
  erreurVitesse = consigneVitesse - vitesse;
  erreurIntegraleVitesse += erreurVitesse * dT_control; //integrale
  rapportCyclique = Kvitesse * erreurVitesse + erreurIntegraleVitesse / Tivitesse; //correction vitesse

  rapportCyclique = constrain(rapportCyclique, -1, 1); //saturation vitesse
  if (rapportCyclique > 0) {
    digitalWrite(moteurSensRotation, LOW);
    analogWrite(moteurMLI, (int)(rapportCyclique * 255));
  } else {
    digitalWrite(moteurSensRotation, HIGH);
    analogWrite(moteurMLI, (int)(-rapportCyclique * 255));
  }
}


void afficher() {
  /*Serial.print("\n Gain = ");
    Serial.println(Kposition);*/

  //Serial.print("La position est (degre):");
  Serial.println(Position);
}


void setup() {
  Serial.begin(baudRate); // initialisation de la liaison série

  timer.setInterval((int)1000 * dT_mesure, mesureVitesse); //en seconde
  timer.setInterval((int)1000 * dT_control, control);
  timer.setInterval((int)1000 * dT_affichage, afficher);

  pinMode(moteurSensRotation, OUTPUT); //initialisation direction moteur A
  pinMode(moteurFrein, OUTPUT); //initialisation frein A
  digitalWrite(moteurSensRotation, LOW); //definie le sens de rotation
  digitalWrite(moteurFrein, LOW);    //enleve le frein


  //modification des registres pour changer la frequence de la MLI
  int myEraser = 7;
  TCCR3B &= ~myEraser;
  int myPrescaler = 2;
  TCCR3B |= myPrescaler;
  attachInterrupt(digitalPinToInterrupt(encoderPinA), encodeMoteur, CHANGE);//trigger encodeMoteur quand encoderPinA change de valeur
}


void loop() {
  timer.run();
  if (Serial.available() > 0) {
    consignePosition = Serial.parseInt(); //demande la consigne
  }
}
