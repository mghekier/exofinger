# exofinger

21/O1/2021

Repository for saving/sharing code for Exofinger hackaton

## Collaborative sites

* [wiki final MHK Exofinger](https://wikilab.myhumankit.org/index.php?title=Projets:Exofinger_:_Thumb)
* [Drive google](https://drive.google.com/drive/folders/1kMVToGUdSXAB_s5LK848k6DMtL1qms5z?usp=sharing)
* [Mattermost interne](https://mattermost.inria.fr/exofinger/channels/general)
* [Wiki interne](https://gitlab.inria.fr/humanlab-inria/doc/-/wikis/home)

## Arduino code

### arduino/journees_rennes

Arduino source code for the glove solution `Version_gant.ino` and the fist solution `Version_poing_armoricain.ino`.

### arduino/pid_clement

Arduino source code of a PID control motor made by Clement

### arduino/sensors_and_grossrc

To prepare the hackaton we have written very simple arduino code for qualifying actuators and sensors. We use an [Arduino Uno](https://store.arduino.cc/arduino-uno-rev3) or [[Arduino Nano](https://store.arduino.cc/arduino-nano) as a controller and a [Grossrc prothestic hand](https://www.thingiverse.com/thing:1691704) to qualify servo-motors.

Find in `sensors_and_grossrc`:

* `ex_servo`: Prothestic Hand open/close example
* `ex_analog`: Analog sensor acquisition like fsr, [emg](https://www.amazon.fr/gp/product/B083S5RJRT/ref=ox_sc_act_title_3) or [sensorflex](https://www.amazon.fr/perfk-Capteur-Arduino-Flexion-Sensor/dp/B07KSBDCFG/ref=sr_1_5)
* `ex_imu`: IMU [AdaFruit BNO055](https://www.adafruit.com/product/2472) example use of a gyrometer to detect a rotation on thresold
* `ex_hand_analog`: Prothestic Hand open/close trigged by an analog input (fsr, emg or sensorflex) activated by an operator
* `ex_hand_sensorflex`: Copy of the flexion of the [sensorflex](https://www.amazon.fr/perfk-Capteur-Arduino-Flexion-Sensor/dp/B07KSBDCFG/ref=sr_1_5) sensor on a servo motor angle
* `ex_hand_button`: Prothestic Hand open/close trigged by push button
* `ex_hand_imu`: Prothestic Hand open/close trigged by an IMU [AdaFruit BNO055](https://www.adafruit.com/product/2472) placed on the hand of an operator
* `ex_stim`: [Odstock](http://www.salisburyfes.com/dropfoot.htm) stim activation.

### Visualisation

`ex_analog` and `ex_imu` comes with python code to log and plot data sensors. Data examples are stored in csv `output`.
To log data, during the experiment, you need to launch `log_xxx.py` (ctl-c to stop the log)

python requirements are : python3 with modules : serial, matplotlib and pandas

### More information

* [Video illustrations](https://drive.google.com/drive/folders/1gbXRlkENGu5STzGmhzN7ylGTX-7uUh0S)
* [Material list](https://docs.google.com/spreadsheets/d/1uTLe2PoLP3MaCZ5nKpEzAiTt0lUUg9b_Fz-YXgz_ZJ4/edit#gid=0)
* [Team Exofinger](https://docs.google.com/spreadsheets/d/1uTLe2PoLP3MaCZ5nKpEzAiTt0lUUg9b_Fz-YXgz_ZJ4/edit#gid=0)
