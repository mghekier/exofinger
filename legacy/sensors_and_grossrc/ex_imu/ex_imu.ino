/**
 * IMU BNO055 example use of a gyrometer
 * to detect a rotation on thresold
 */
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>


#define THRESHOLD 100.00

boolean Motion = false;

/* This driver reads raw data from the BNO055

   Connections
   ===========
   Connect SCL to analog 5
   Connect SDA to analog 4
   Connect VDD to 3.3V DC
   Connect GROUND to common ground

   History
   =======
   2015/MAR/03  - First release (KTOWN)
*/

/* Set the delay between fresh samples */
#define BNO055_SAMPLERATE_DELAY_MS (100)

// Check I2C device address and correct line below (by default address is 0x29 or 0x28)
//                                   id, address
Adafruit_BNO055 bno = Adafruit_BNO055(-1, 0x28);

void displaySensorDetails(void)
{
  sensor_t sensor;
  bno.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value);
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); 
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); 
  Serial.println("------------------------------------");
  Serial.println("");
  delay(500);
}


/**************************************************************************/
/*
    Arduino setup function (automatically called at startup)
*/
/**************************************************************************/
void setup(void)
{
  Serial.begin(115200);
  Serial.println("Orientation Sensor Raw Data Test"); Serial.println("");

  /* Initialise the sensor */
  if(!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while(1);
  }

  delay(1000);

  /* Display the current temperature */
  int8_t temp = bno.getTemp();
  Serial.print("Current Temperature: ");
  Serial.print(temp);
  Serial.println(" C");
  Serial.println("");

  bno.setExtCrystalUse(true);

  Serial.println("Calibration status values: 0=uncalibrated, 3=fully calibrated");
  displaySensorDetails();
}

/**************************************************************************/
/*
    Arduino loop function, called once 'setup' is complete (your own code
    should go here)
*/
/**************************************************************************/
void loop(void)
{
  // Possible vector values can be:
  // - VECTOR_ACCELEROMETER - m/s^2
  // - VECTOR_MAGNETOMETER  - uT
  // - VECTOR_GYROSCOPE     - rad/s
  // - VECTOR_EULER         - degrees
  // - VECTOR_LINEARACCEL   - m/s^2
  // - VECTOR_GRAVITY       - m/s^2
  imu::Vector<3> acc = bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL);
  imu::Vector<3> gyr = bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE);


  /* Display ACCELEROMETER */
  Serial.print("IMU T: ");
  Serial.print(millis());
  Serial.print(" AX: ");
  Serial.print(acc.x());
  Serial.print(" AY: ");
  Serial.print(acc.y());
  Serial.print(" AZ: ");
  Serial.print(acc.z());
  Serial.println("");

  /* Display GYROMETER*/
  Serial.print("IMU T: ");
  Serial.print(millis());
  Serial.print(" GX: ");
  Serial.print(gyr.x());
  Serial.print(" GY: ");
  Serial.print(gyr.y());
  Serial.print(" GZ: ");
  Serial.print(gyr.z());
  Serial.println("");

  if ( (gyr.y() > THRESHOLD) && (Motion == false) ) {
    Motion = true;
    Serial.println("------------- Motion detected !");
  }
  else if ( (gyr.y() < THRESHOLD) && (Motion == true) ) {
    Motion = false;
  }

  delay(BNO055_SAMPLERATE_DELAY_MS);
}
