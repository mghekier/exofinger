import serial
import re
import sys
import pandas as pd
#import matplotlib as plt

baud = 115200 
DATALOG = []
rawlog = "output/log_raw.txt"
acclog = "output/log_imu_acc.csv"
gyrlog = "output/log_imu_gyr.csv"

def saveLog(filelog, DATA):
    datafile = open(filelog, 'w')
    for line in DATA:
        datafile.write(str(line)+"\n")
    datafile.close()

def extractImuAcc(DATA):
    df = pd.DataFrame(columns=['Time', 'AX', 'AY', 'AZ'])
    i = 0
    for line in DATA:
        m = re.search('^IMU T:(.+?) AX:(.+?) AY:(.+?) AZ:(.+?)$', line)
        if m:
            df.loc[i] = [float(m.group(1))/1000.0, float(m.group(2)), float(m.group(3)), float(m.group(4))]
            i = i + 1
    return df

def extractImu(DATA):
    df_acc = pd.DataFrame(columns=['Time', 'AX', 'AY', 'AZ'])
    df_gyr = pd.DataFrame(columns=['Time', 'GX', 'GY', 'GZ'])
    i_acc = 0
    i_gyr = 0
    for line in DATA:
        m_acc = re.search('^IMU T:(.+?) AX:(.+?) AY:(.+?) AZ:(.+?)$', line)
        m_gyr= re.search('^IMU T:(.+?) GX:(.+?) GY:(.+?) GZ:(.+?)$', line)
        if m_acc:
            df_acc.loc[i_acc] = [float(m_acc.group(1))/1000.0, float(m_acc.group(2)), float(m_acc.group(3)), float(m_acc.group(4))]
            i_acc = i_acc + 1
        elif m_gyr:
            df_gyr.loc[i_gyr] = [float(m_gyr.group(1))/1000.0, float(m_gyr.group(2)), float(m_gyr.group(3)), float(m_gyr.group(4))]
            i_gyr = i_gyr + 1
    return df_acc, df_gyr


if __name__ == "__main__":

    arduino = serial.Serial('/dev/ttyACM0', baud, timeout=.1)
    print('Connexion a ' + arduino.name + ' a un baud rate de ' + str(baud))

    log = False
    while True:
        try:
            data = arduino.readline()[:-2].decode("utf-8")
            if data:
                if data == "Orientation Sensor Raw Data Test": log = True
                if log:
                    DATALOG.append(data)
                print(data)
        except KeyboardInterrupt:
            print("Bye")
           
            saveLog(rawlog, DATALOG)
            df_acc, df_gyr = extractImu(DATALOG)
            df_acc.to_csv(acclog, index = False)
            df_gyr.to_csv(gyrlog, index = False)
            
            sys.exit()
        

