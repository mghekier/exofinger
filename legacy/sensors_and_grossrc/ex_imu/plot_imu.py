import pandas as pd
import matplotlib.pyplot as plt

acclog = "output/log_imu_acc.csv"
gyrlog = "output/log_imu_gyr.csv"

df_acc = pd.read_csv(acclog)  
df_gyr = pd.read_csv(gyrlog)  

df_acc.plot(x='Time', y=["AX", "AY", "AZ"], grid = True, title = 'Accelerometers')
df_gyr.plot(x='Time', y=["GX", "GY", "GZ"], grid = True, title = 'Gyrometers')
plt.show()


print(df_acc.head())