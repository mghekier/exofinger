/**
 * Prothestic Hand open/close trigged by an BNO055 IMU
 *  placed on the hand of an operator
 */
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include "Hand.h"

/* TEMPO defining the open/close velocity */
#define TEMPO 10

/* Gyrometer threshold on axis Y trigging hand open-close */
#define THRESHOLD 100.00

/* Set the delay between fresh samples */
#define BNO055_SAMPLERATE_DELAY_MS (100)

// Check I2C device address and correct line below (by default address is 0x29 or 0x28)
//                                   id, address
Adafruit_BNO055 bno = Adafruit_BNO055(-1, 0x28);

int hand_state;
Hand myHand(9, 8, 120, 180, true);

boolean Motion = false;

void setup()
{
  Serial.begin(115200);
  /* Initialize the prothestic hand */
  myHand.init();
  myHand.setposHand(0, 0);
  hand_state = myHand.getState();

  /* Initialise the sensor */
  if (!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while (1)
      ;
  }

  delay(1000);

  /* Display the current temperature */
  int8_t temp = bno.getTemp();
  Serial.print("Current Temperature: ");
  Serial.print(temp);
  Serial.println(" C");
  Serial.println("");

  bno.setExtCrystalUse(true);
}

void loop()
{
  hand_state = myHand.getState();

  imu::Vector<3> gyr = bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE);

  /* Display GYROMETER
  Serial.print("IMU T: ");
  Serial.print(millis());
  Serial.print(" GX: ");
  Serial.print(gyr.x());
  Serial.print(" GY: ");
  Serial.print(gyr.y());
  Serial.print(" GZ: ");
  Serial.print(gyr.z());
  Serial.println("");
  */

  if ((gyr.y() > THRESHOLD) && (Motion == false))
  {
    Motion = true;
    Serial.println("------------- Motion detected !");
    if (hand_state == OPEN)
    {
      Serial.println("------------- Close hand !");
      myHand.moveHand(CLOSE, TEMPO);
    }
    else if (hand_state == CLOSE)
    {
      Serial.println("------------- Open hand !");
      myHand.moveHand(OPEN, TEMPO);
    }
  }
  else if ((gyr.y() < THRESHOLD) && (Motion == true))
  {
    Motion = false;
  }

  delay(BNO055_SAMPLERATE_DELAY_MS);
}
