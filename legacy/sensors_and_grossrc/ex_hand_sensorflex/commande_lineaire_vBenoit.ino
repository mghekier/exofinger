#include <Servo.h>
//#include <Filter.h>

//attribution des pins
const char flexPin = A0;
const int pinServo = 10;

// the <float> makes a filter for float numbers
// 20 is the weight (20 => 20%)
// 0 is the initial value of the filter
//ExponentialFilter<float> FilteredFlexValue(20, 0);

// variable propre à la flexion
int flexLevel;
// variables propres au moteur
Servo myservo;

void setup ()
{
  myservo.attach(pinServo);
  Serial.begin(9600);
}

void loop()
{
  //données de flexion
  flexLevel = analogRead(flexPin);
  
//  FilteredFlexValue.Filter(flexLevel);
//  SmoothFlexValue = FilteredFlexValue.Current();
  
  flexLevel = map(flexLevel, 129, 200, 90, 0);
  flexLevel = constrain(flexLevel, 0, 90);
  Serial.println(flexLevel);
  myservo.write(flexLevel);

}
