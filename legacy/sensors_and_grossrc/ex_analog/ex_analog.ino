/**
 * Analog sensor acquisition
 * like fsr, emg or sensorflex
 */

#define INPUT_ANA 0

// variable to store the value read
int ana_val = 0;

void setup()
{
    //  setup serial
    Serial.begin(115200);
}

void loop()
{
    ana_val = analogRead(INPUT_ANA);
    Serial.println(ana_val);
}