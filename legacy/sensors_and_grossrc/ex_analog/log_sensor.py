import serial
import argparse
import re
import sys
import pandas as pd
#import matplotlib as plt

baud = 115200 
DATALOG = []

def extractSensor(DATA):
    df = pd.DataFrame(columns=['sensor'])
    i = 0
    for line in DATA:
        df.loc[i] = [int(line)]
        i = i + 1
    return df


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Plot sensor log [filename=(output/log_sensor.csv by default)]")
    parser.add_argument("filename", nargs='?', help="Sensor log filename in cvs format")
    args = parser.parse_args()

    if args.filename == None:
        sensorlog = "output/log_sensor.csv"
    else:
        sensorlog = args.filename


    arduino = serial.Serial('/dev/ttyACM0', baud, timeout=.1)
    print('Connexion a ' + arduino.name + ' a un baud rate de ' + str(baud))

    while True:
        try:
            data = arduino.readline()[:-2].decode("utf-8")
            if data:
                print(data)
                DATALOG.append(data)
        except KeyboardInterrupt:
            print("Bye")
            df_sensor = extractSensor(DATALOG)
            df_sensor.to_csv(sensorlog, index = False)  
            sys.exit()
        

