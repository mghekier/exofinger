import argparse
import pandas as pd
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="Plot sensor log [filename=(output/log_sensor.csv by default)]")
parser.add_argument("filename", nargs='?', help="Sensor log filename in cvs format")
args = parser.parse_args()

if args.filename == None:
    sensorlog = "output/log_sensor.csv"
else:
    sensorlog = args.filename

print("OPTION ", sensorlog)

df_sensor = pd.read_csv(sensorlog)  

df_sensor.plot(y = 'sensor', grid = True, title = 'Sensor')
plt.show()
