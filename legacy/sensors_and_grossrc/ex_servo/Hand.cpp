#include "Hand.h"

Hand::Hand(int pinThumb, int pinPalm, int maxThumb, int maxPalm, bool verbose)
{
    _pinThumb = pinThumb;
    _pinPalm = pinPalm;
    _maxThumb = maxThumb;
    _maxPalm = maxPalm;
    _verbose = verbose;
}

void Hand::init()
{
    _servoThumb.attach(_pinThumb);
    _servoPalm.attach(_pinPalm);
    _state = OPEN;
}

int Hand::getState()
{
    return _state;
}

void Hand::setState(int mode)
{
    _state = mode;
}

void Hand::setposThumb(int pos)
{
    int satpos = min(satpos, _maxThumb);
    _servoThumb.write(pos);
}

void Hand::setposPalm(int pos)
{
    int satpos = min(pos, _maxPalm);
    _servoPalm.write(satpos);
}

void Hand::setposHand(int posThumb, int posPalm)
{
    setposThumb(posThumb);
    setposPalm(posPalm);
}

void Hand::moveHand(int mode, int tempo)
{
    switch (mode)
    {
    case CLOSE:
        if (_verbose)
            Serial.println("Hand:: Close");
        if (_state != CLOSE)
        {
            for (int position = 0; position <= 180; position++)
            {
                setposHand(position, position);
                delay(tempo);
            }
            _state = CLOSE;
        }
        else
        {
            Serial.println("Hand error:: Hand already close");
        }

        break;
    case OPEN:
        if (_verbose)
            Serial.println("Hand:: Open");
        if (_state != OPEN)
        {
            for (int position = 180; position >= 0; position--)
            {
                setposHand(position, position);
                delay(tempo);
            }
            _state = OPEN;
        }
        else
        {
            Serial.println("Hand error:: Hand already open");
        }
        break;
    default:
        break;
    }
}