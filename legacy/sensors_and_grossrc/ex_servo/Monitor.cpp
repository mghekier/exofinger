#include "Monitor.h"

Monitor::Monitor(long baudSpeed)
{
    _baudSpeed = baudSpeed;
    _cmd = CMD_NONE;
}

void Monitor::init()
{
    Serial.begin(_baudSpeed);
}

void Monitor::resetOrder() 
{
    _cmd = CMD_NONE;
}

int Monitor::getOrder(int state)
{
    _state = state;
    _msgOrder = Serial.readStringUntil('\n');
   
    if (_msgOrder.equals("open"))
    {
        _cmd = CMD_OPEN;
    }
    else if (_msgOrder.equals("close"))
    {
        _cmd = CMD_CLOSE;
    }
    else if (_msgOrder.equals("stop"))
    {
        _cmd = CMD_STOP;
    }
    else if (_msgOrder.equals("help"))
    {
        Serial.println("open/close/stop");
    } else {
        _cmd = CMD_NONE;
    }
    Serial.println((String) "GET ORDER " + _cmd);

    return _cmd;
}