#ifndef Hand_h
#define Hand_h

#include <Arduino.h>
#include <Servo.h>

#define OPEN  1
#define CLOSE 0

class Hand
{
public:
    Hand(int pinThumb, int pinPalm, int maxThumb, int maxPalm, bool verbose);
    void init();
    void setposThumb(int pos);
    void setposPalm(int pos);
    void setposHand(int posThumb, int posPalm);
    void moveHand(int mode, int tempo);
    int  getState();
    void setState(int mode);

private:
    int _pinThumb;
    int _pinPalm;
    int _maxThumb;
    int _maxPalm;
    int _verbose;
    int _state;
    Servo _servoThumb;
    Servo _servoPalm;
};

    
#endif 