/**
 * Prothestic Hand open/close example
 */

#include "Hand.h"
#include "Monitor.h"

#define TEMPO 5

int cmd;
int hand_state;
Hand myHand(2, 3, 120, 180, true);
Monitor myMonitor(115200);

void setup()
{
  cmd = CMD_NONE;
  myMonitor.init();
  myHand.init();
  myHand.setposHand(0, 0);
  hand_state = myHand.getState();
}

void loop()
{

  hand_state = myHand.getState();
  cmd = myMonitor.getOrder(hand_state);

  switch (cmd)
  {
  case CMD_CLOSE:
    myHand.moveHand(CLOSE, TEMPO);
    break;
  case CMD_OPEN:
    myHand.moveHand(OPEN, TEMPO);
    break;
  }
}
