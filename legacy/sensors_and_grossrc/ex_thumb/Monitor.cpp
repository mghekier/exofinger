#include "Monitor.h"

Monitor::Monitor(long baudSpeed)
{
    _baudSpeed = baudSpeed;
    _cmd = CMD_NONE;
}

void Monitor::init()
{
    Serial.begin(_baudSpeed);
}

void Monitor::resetOrder() 
{
    _cmd = CMD_NONE;
}

int Monitor::getOrder(int state)
{
    _state = state;
    _msgOrder = Serial.readStringUntil('\n');
   
    if (_msgOrder.equals("pull"))
    {
        _cmd = CMD_PULL;
    }
    else if (_msgOrder.equals("push"))
    {
        _cmd = CMD_PUSH;
    }
    else if (_msgOrder.equals("stop"))
    {
        _cmd = CMD_STOP;
    }
    else if (_msgOrder.equals("help"))
    {
        Serial.println("pull/push/stop");
    } else {
        _cmd = CMD_NONE;
    }
    //Serial.println((String) "GET ORDER " + _cmd);

    return _cmd;
}