#include "Thumb.h"
#include "Monitor.h"

Thumb::Thumb(int pinThumb,  int maxThumb, bool verbose)
{
    _pinThumb = pinThumb;
    _maxThumb = maxThumb;
    _verbose = verbose;
}

void Thumb::init()
{
    _servoThumb.attach(_pinThumb);
    _state = PULLED;
}

int Thumb::getState()
{
    return _state;
}

void Thumb::setState(int mode)
{
    _state = mode;
}

void Thumb::setposThumb(int pos)
{
    int satpos = min(satpos, _maxThumb);
    _servoThumb.write(pos);
}


void Thumb::moveThumb(int mode, int tempo)
{
    switch (mode)
    {
    case CMD_PUSH:
        if (_verbose)
            Serial.println("Thumb:: Push");
        if (_state != PUSHED)
        {
            for (int position = 0; position <= 180; position++)
            {
                setposThumb(position);
                delay(tempo);
                //Serial.print('pos+');
                //Serial.println(position);
            }
            _state = PUSHED;
        }
        else
        {
            Serial.println("Thumb error:: Thumb already pushed");
        }

        break;
    case CMD_PULL:
        if (_verbose)
            Serial.println("Thumb:: Pull");
        if (_state != PULLED)
        {
            for (int position = 180; position >= 0; position--)
            {
                setposThumb(position);  
                delay(tempo);
                //Serial.println('pos-', position);
            }
            _state = PULLED;
        }
        else
        {
            Serial.println("Thumb error:: Thumb already open");
        }
        break;
    default:
        break;
    }
}