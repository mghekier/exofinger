#ifndef Hand_h
#define Hand_h

#include <Arduino.h>
#include <Servo.h>

#define PULLED  1
#define PUSHED  0

class Thumb
{
public:
    Thumb(int pinThumb, int maxThumb, bool verbose);
    void init();
    void setposThumb(int pos);
    void moveThumb(int mode, int tempo);
    int  getState();
    void setState(int mode);

private:
    int _pinThumb;
    int _maxThumb;
    int _verbose;
    int _state;
    Servo _servoThumb;
};

    
#endif 