/**
 * Odstock stim activation. Depending of the chip used for
 * interfacing the Arduino board the activation is done by:
 *  - a rising front with a LBB110
 *  - a falling front with a G3VM
 */

#include "Monitor.h"

#define OUTPUT_STIM 6
#define ACTIVE HIGH
#define NO_ACTIVE LOW

int cmd;

Monitor myMonitor(115200);

void setup()
{
  myMonitor.init();
  pinMode(OUTPUT_STIM, OUTPUT);
  digitalWrite(OUTPUT_STIM, NO_ACTIVE); 
  Serial.println("Stim Odstock activation example");
}

void loop()
{

  cmd = myMonitor.getOrder();

  switch (cmd)
  {
  case CMD_STIM:
    Serial.print("Stim...");
    digitalWrite(OUTPUT_STIM, ACTIVE); 
    //myHand.moveHand(CLOSE, TEMPO);
    delay(2000);
    digitalWrite(OUTPUT_STIM, NO_ACTIVE); 
    Serial.println("End");
    break;

  }
}
