#include "Monitor.h"

Monitor::Monitor(long baudSpeed)
{
    _baudSpeed = baudSpeed;
    _cmd = CMD_NONE;
}

void Monitor::init()
{
    Serial.begin(_baudSpeed);
}

void Monitor::resetOrder() 
{
    _cmd = CMD_NONE;
}

int Monitor::getOrder()
{
    _msgOrder = Serial.readStringUntil('\n');
   
    if (_msgOrder.equals("go"))
    {
        _cmd = CMD_STIM;
    }
    else if (_msgOrder.equals("help"))
    {
        Serial.println("go");
    } else {
        _cmd = CMD_NONE;
    }
    Serial.println((String) "GET ORDER " + _cmd);

    return _cmd;
}