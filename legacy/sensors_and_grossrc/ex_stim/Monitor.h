#ifndef Monitor_h
#define Order_h

#define LOW_BAUDRATE 0
#define MEDIUM_BAUDRATE 1
#define HIGH_BAUDRATE 2

#include <Arduino.h>

#define CMD_NONE -1
#define CMD_STIM  0

class Monitor
{
public:
    Monitor(long baudSpeed);
    void init();
    int getOrder();
    void resetOrder();
  
private:
    long _baudSpeed;
    int _cmd;
    String _msgOrder;
};
#endif
