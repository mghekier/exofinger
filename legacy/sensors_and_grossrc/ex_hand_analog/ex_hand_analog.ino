/**
 * Prothestic Hand open/close trigged by an analog
 *  input (fsr, emg or sensorflex) activated by an operator
 */
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include "Hand.h"

/* Pin analog input */
/* Analog Sensor could be: fsr, emg or sensorflex */
#define INPUT_ANA 0

/* TEMPO defining the open/close velocity */
#define TEMPO 10

/* Analog threshold  */
#define THRESHOLD_ANA 350.00

int hand_state;
Hand myHand(9, 8, 120, 180, true);
boolean Motion = false;
int val_fsr;

void setup()
{
  Serial.begin(115200);
  /* Initialize the prothestic hand */
  myHand.init();
  myHand.setposHand(0, 0);
  hand_state = myHand.getState();

}

void loop()
{
  hand_state = myHand.getState();

  val_fsr = analogRead(INPUT_ANA);

  if ((val_fsr > THRESHOLD_ANA) && (Motion == false))
  {
    Motion = true;
    Serial.println("------------- Motion detected !");
    if (hand_state == OPEN)
    {
      Serial.println("------------- Close hand !");
      myHand.moveHand(CLOSE, TEMPO);
    }
    else if (hand_state == CLOSE)
    {
      Serial.println("------------- Open hand !");
      myHand.moveHand(OPEN, TEMPO);
    }
  }
  else if ((val_fsr < THRESHOLD_ANA) && (Motion == true))
  {
    Motion = false;
  }

  delay(100);
}
