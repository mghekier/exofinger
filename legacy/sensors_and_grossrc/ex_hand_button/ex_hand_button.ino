
/**
 * Prothestic Hand open/close trigged by push button
 */

#include "Hand.h"

/* Pin button digital input */
#define INPUT_BUTTON 2
/* TEMPO defining the open/close velocity */
#define TEMPO 10

/* position of the button */
int button = 0;
boolean Motion = false;
int hand_state;
Hand myHand(9, 8, 120, 180, true);

void setup()
{

    Serial.begin(115200);
    /* activate pull-up internal resistor */
    pinMode(INPUT_BUTTON, INPUT_PULLUP);
    myHand.init();
    myHand.setposHand(0, 0);
    hand_state = myHand.getState();

    Serial.println("Demo open/close hand activated by a push button");
}

void loop()
{
    hand_state = myHand.getState();

    button = digitalRead(INPUT_BUTTON);

    if ((button == LOW) && (Motion == false))
    {
        Motion = true;
        if (hand_state == OPEN)
        {
            Serial.println("------------- Close hand !");
            myHand.moveHand(CLOSE, TEMPO);
        }
        else if (hand_state == CLOSE)
        {
            Serial.println("------------- Open hand !");
            myHand.moveHand(OPEN, TEMPO);
        }
    }
    else if ((button == HIGH) && (Motion == true))
    {
        Motion = false;
    }
}