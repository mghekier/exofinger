# Controle commande

## Solution Gant

### Motorisation

Les principales caractéristiques du moteur linéaire [Actuonix l12-30-100-6-r](https://docs.rs-online.com/ef25/0900766b81682dc2.pdf) sont:
* servo-moteur
* course : 30 mm
* Force max : 42 N
* vitesse max : 13 mm/s
* Alimenatation : 6v/460mA

### Electronique

Le controleur du moteur est constitué d'un micro-controleur [seeduino Nano](https://www.seeedstudio.com/Seeeduino-Nano-p-4111.html), un clone de Arduino Nano, qui commande le moteur linéaire, en lisant l'état d'un interrupteur poussoir, pour tirer ou pousser le cable connecté au pouce.


Dessin de Christophe

### Logiciel

* Version_gant.ino

## Solution Point Américain

### Motorisation

Les principales caractéristiques du moteur linéaire [Actuonix PQ-12-30-12-P](https://www.actuonix.com/Actuonix-PQ-12-P-Linear-Actuator-p/pq12-p.htm) sont:
* moteur courant continu
* course : 20 mm
* Force max : 18 N
* vitesse max : 28 mm/s
* Alimenatation : 12v/210mA

### Electronique 

C'est le même principe que pour la première solution : le micro-controleur [seeduino Nano](https://www.seeedstudio.com/Seeeduino-Nano-p-4111.html) qui commande le moteur linéaire, en lisant l'état d'un interrupteur poussoir, pour tirer ou pousser le cable connecté au pouce.

Comme dans ce cas, le moteur est à courant continu, nous utilisons une carte d'interface de puissance [Polulu DRV88-38](https://www.pololu.com/product/2990) et le contrôle de position est assuré par le logiciel du micro-controleur.

Dessin de Christophe

### Logiciel

* Version_poing_armoricain.ino
